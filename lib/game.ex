defmodule Game do
    use GenServer
    def start_link() do
        secret_word = Dictionary.random_word |> String.trim
        GenServer.start_link(__MODULE__, secret_word)
    end

    def init(word) do
        {:ok, {word, "", "", 9}}
    end

    def submit_guess(pid, letter) do
        GenServer.cast(pid, {:submit_guess, letter})
    end

    def get_feedback(pid) do
        GenServer.call(pid, {:get_feedback})
    end

    def handle_cast({:submit_guess, letter}, state) do
        {:noreply, Hangman.score_guess(state, letter)}
    end

    def handle_call({:get_feedback}, _from, {_,_,_,turns} = state) do
        {:reply, %{
            feedback: Hangman.format_feedback(state),
            remaining_turns: turns,
            status: :playing
        }, state}
    end
end