defmodule Hangman do
def score_guess({word, corrects, incorrects, intents}, guess) when intents == 0 do
  {word, corrects, incorrects, intents}
end
def score_guess({word, corrects, incorrects, intents}, guess)do
if intents == 0 do
  {word, corrects, incorrects, intents}
else
  listOfChars = String.graphemes(word)
  if Enum.member?(listOfChars, guess) do
    listOfCorrects = String.graphemes(corrects)
    if Enum.member?(listOfCorrects, guess) do
      {word, corrects, incorrects, intents}
    else
      {word, corrects<>guess, incorrects, intents}
    end
  else
    listOfIncorrects = String.graphemes(incorrects)
    if Enum.member?(listOfIncorrects, guess) do
      {word, corrects, incorrects, intents}
    else
      {word, corrects, incorrects<>guess, intents - 1}
    end
  end
end
end
def format_feedback({word, correct, _incorrect, _intents})do
listOfChars = String.graphemes(word)
listOfCorrect = String.graphemes(correct)
get_word(listOfChars, listOfCorrect, "")
end
defp get_word([word_head|word_tail], guess, actualWord)do
cond do
  Enum.member?(guess, word_head) == true ->
    actualWord = actualWord<>word_head
    get_word(word_tail, guess, actualWord)
  Enum.member?(guess, word_head) == false ->
    actualWord = actualWord<>"-"
    get_word(word_tail, guess, actualWord)
end
end
defp get_word([], _guess, actualWord)do
actualWord
end
end