defmodule GameRouter do
    use Plug.Router
  
    plug :match
    plug Plug.Parsers, parsers: [:json], pass: ["application/json"], json_decoder: Poison
    plug :dispatch
  
    def start_link do
      Plug.Adapters.Cowboy2.http(GameRouter, [])
    end
    
    match _ do
      send_resp(conn, 404, "Oops")
    end
  end
