defmodule GameTest do
    use ExUnit.Case
    import Mock
  
    test "Creates a process holding the game state" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
  
        assert is_pid(pid)
        assert pid != self()
        assert called Dictionary.random_word
      end
    end
  
    test "Submit a guess for the secret word" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert :ok = Game.submit_guess(pid,"t")
      end
    end
  
    test "get feedback on our guesses - status playing" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        Game.submit_guess(pid,"t")
        Game.submit_guess(pid,"p")
        Game.submit_guess(pid,"q")
        assert %{feedback: "p--t-p--", remaining_turns: 8, status: :playing} = Game.get_feedback(pid)
      end
    end

    # some other tests follow
    test "get feedback on our guesses - lose" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        Game.submit_guess(pid,"f")
        Game.submit_guess(pid,"g")
        Game.submit_guess(pid,"q")
        Game.submit_guess(pid,"q")
        Game.submit_guess(pid,"k")
        Game.submit_guess(pid,"v")
        Game.submit_guess(pid,"w")
        Game.submit_guess(pid,"m")
        Game.submit_guess(pid,"n")
        Game.submit_guess(pid,"r")
        assert %{feedback: "--------", remaining_turns: 0, status: :lose} = Game.get_feedback(pid)
      end
    end
    # test "get feedback on our guesses - win" do
    #   with_mock Dictionary, [random_word: fn() -> "platypus" end] do
    #     {:ok, pid} = Game.start_link()
    #     Game.submit_guess(pid,"p")
    #     Game.submit_guess(pid,"x")
    #     Game.submit_guess(pid,"l")
    #     Game.submit_guess(pid,"a")
    #     Game.submit_guess(pid,"t")
    #     Game.submit_guess(pid,"y")
    #     Game.submit_guess(pid,"u")
    #     Game.submit_guess(pid,"s")
  
    #     assert %{feedback: "platypus", remaining_turns: 8, status: :win} = Game.get_feedback(pid)
    #   end
    # end
  end
  